# Saiwa's dotfiles

Below are the pieces of software I used:

- X.org as the display server
- i3 as the window manager
- i3status as the status bar
- rofi as a dmenu replacement
- vim as the text editor
- zathura as the document viewer
- feh as the image viewer
- lf as the file explorer

I also refer to Iosevka font (nerd font) in most of my files and I use https://github.com/dracula/qutebrowser-dracula-theme.git as the qutebrowser theme 
