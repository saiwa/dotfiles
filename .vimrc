set number
set tabstop=2
set shiftwidth=2
set expandtab
syntax on
syntax enable
map <F5> :NERDTreeToggle<CR>
call plug#begin()
Plug 'preservim/NERDTree'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
"Plug 'https://github.com/ap/vim-css-color'
Plug 'tpope/vim-fugitive'
Plug 'morhetz/gruvbox'
Plug 'mattn/emmet-vim'
call plug#end()
set background=dark
set t_Co=256
"let g:airline_extensions = []
let g:airline#extensions#cursormode#enabled = 0
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_skip_empty_sections = 1
let g:airline_powerline_fonts = 1
let g:airline_theme = 'gruvbox'
let g:user_emmet_settings = {
\  'variables': {'lang': 'it'},
\  'html': {
\    'default_attributes': {
\      'option': {'value': v:null},
\      'textarea': {'id': v:null, 'name': v:null, 'cols': 10, 'rows': 10},
\    },
\    'snippets': {
\      'html:5': "<!DOCTYPE html>\n"
\              ."<html lang=\"${lang}\">\n"
\              ."<head>\n"
\              ."\t<meta charset=\"${charset}\">\n"
\              ."\t<title></title>\n"
\              ."\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
\              ."</head>\n"
\              ."<body>\n\t${child}|\n</body>\n"
\              ."</html>",
\    },
\  },
\}
colorscheme gruvbox

