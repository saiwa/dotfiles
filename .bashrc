#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias pacman='sudo pacman'

export GPG_TTY=$(tty)
export EDITOR='vim'

PS1='[\u@\h \W]\$ '

#if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
#  setxkbmap it
#  startx
#fi
