import dracula.draw

config.load_autoconfig();
dracula.draw.blood(c, {
    'spacing': {
        'vertical': 2,
        'horizontal': 8
    }
})
config.set('qt.args', ['disable-seccomp-filter-sandbox', 'force-dark-mode'])
config.set('fonts.default_family', 'Iosevka')
config.set('fonts.default_size', '16px')
#config.set('colors.webpage.darkmode.enabled', True)
config.set('fonts.contextmenu', '16pt Iosevka')
config.set('colors.contextmenu.menu.bg', '#282a36')
config.set('colors.contextmenu.menu.fg', '#ffffff')
config.set('colors.contextmenu.selected.bg', '#44475a')
config.set('colors.contextmenu.selected.fg', '#ffffff')
config.set('fonts.web.family.fixed', 'Iosevka')
config.set('fonts.web.size.default_fixed', 16)
